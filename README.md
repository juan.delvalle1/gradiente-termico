# Gradiente Termico
Gradiente Térmico es un programa que busca simular de manera visual el comportamiento de la temperatura y el calor en una superficie cuando busca el equilibrio térmico. Esto es con fines meramente ilustrativos y no supone un simulador preciso y adecuado.

## Requisitos
QT Creator 5.12.1 o superior
<br>
MinGW o compilador equivalente.
<br>
QTCharts
<br>
Una vez instaladas todas estas dependencias (Se pueden instalar mediante el wizard de QTCreator) Pasamos a la ejecución

## Compilación
### Linux y Windows

Tras clonar el presente repositorio, abrir el archivo .pro en QTCreator presionar el botón RUN.

Una vez que la pestaña aparece solo se debe mover los diales para cambiar el vector que se esta evaluando y ver como se mueve la temperatura
según los colores siendo rojo el más cálido y azul el más frío.



<br>
Miembros:
<br>
Javiera Alvarez
<br>
Mauricio Carvajal
<br>
Juan Pablo del Valle
<br>
Tomas Garreton
