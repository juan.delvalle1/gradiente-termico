#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QObject>
#include "rectanglecolor.h"
#include "circlecolor.h"
#include "elipsecolor.h"
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
public slots:
  void dialValueChanged();

public:
    Ui::Widget *ui;
    QGraphicsScene *scene1;
    QGraphicsScene *scene2;
    QGraphicsScene *scene3;
    RectangleColor *rect;
    CircleColor *circle;
    ConicalColor *cone;
//private slots:
    //void on_ui_windowTitleChanged(const QString &title);
};

#endif // WIDGET_H
