#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    scene1 = new QGraphicsScene(this);
    scene2 = new QGraphicsScene(this);
    scene3 = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene1);
    ui->graphicsView_2->setScene(scene2);
    ui->graphicsView_3->setScene(scene3);
    rect = new RectangleColor(0,0,200,200);
    circle= new CircleColor(0,0,200,200);
    cone= new ConicalColor(0,0,200,200);
    scene1->addItem(rect);
    scene2->addItem(circle);
    scene3->addItem(cone);
    connect(ui->dialRojo, SIGNAL(valueChanged(int)), this , SLOT(dialValueChanged(int)));
    connect(ui->dialVerde, SIGNAL(valueChanged(int)), this , SLOT(dialValueChanged(int)));
    connect(ui->dialRojo,SIGNAL(valueChanged(int)),rect,SLOT(changeDireccionX(int)));
    connect(ui->dialVerde,SIGNAL(valueChanged(int)),rect,SLOT(changeDireccionY(int)));
    connect(ui->dialRojo_2,SIGNAL(ValueChanged(int)),this , SLOT(dialValueChanged(int)));
    connect(ui->dialVerde_2,SIGNAL(ValueChanged(int)),this , SLOT(dialValueChanged(int)));
    connect(ui->dialRojo_2,SIGNAL(ValueChanged(int)),circle , SLOT(changeDireccionX(int)));
    connect(ui->dialVerde_2,SIGNAL(ValueChanged(int)),circle , SLOT(changeDireccionY(int)));
    connect(ui->dialRojo_2,SIGNAL(ValueChanged(int)),circle , SLOT(changeRadial(int)));
    connect(ui->dialVerde_2,SIGNAL(ValueChanged(int)),circle , SLOT(changeRadial(int)));
    connect(ui->dialRojo_3,SIGNAL(ValueChanged(int)),this , SLOT(dialValueChanged(int)));
    connect(ui->dialVerde_3,SIGNAL(ValueChanged(int)),this , SLOT(dialValueChanged(int)));
    connect(ui->dialRojo_3,SIGNAL(ValueChanged(int)),cone , SLOT(changeDireccionX(int)));
    connect(ui->dialVerde_3,SIGNAL(ValueChanged(int)),cone , SLOT(changeDireccionY(int)));
    connect(ui->dialRojo_3,SIGNAL(ValueChanged(int)),cone , SLOT(changeAngle(int)));
    connect(ui->dialVerde_3,SIGNAL(ValueChanged(int)),cone , SLOT(changeAngle(int)));
}

void Widget::dialValueChanged()
{
    ui->dialRojo->setMinimum(-400);
    ui->dialRojo->setMaximum(400);
    ui->dialVerde->setMinimum(-400);
    ui->dialVerde->setMaximum(400);
    ui->dialRojo_2->setMinimum(-400);
    ui->dialRojo_2->setMaximum(400);
    ui->dialVerde_2->setMinimum(-400);
    ui->dialVerde_2->setMaximum(400);
    ui->dialRojo_3->setMinimum(-400);
    ui->dialRojo_3->setMaximum(400);
    ui->dialVerde_3->setMinimum(-400);
    ui->dialVerde_3->setMaximum(400);
}


Widget::~Widget()
{
    delete ui;
    delete scene1;
    delete scene2;
    delete rect;
    delete circle;
    delete cone;
}
