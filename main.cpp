#include "widget.h"
#include <QApplication>
#include "fadbad.h"
#include "fadiff.h"
#include "tadiff.h"
#include <iostream>
#include <vector>
#include <math.h>
#include <rectanglecolor.h>
#include <circlecolor.h>
#include <elipsecolor.h>

using namespace std;
using namespace fadbad;


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    double posx= 0;
    double posy= 0;
    double dirX=200;
    double dirY=200;


    Widget r;
    RectangleColor w(posx,posy,dirX,dirY);
    CircleColor c(posx,posy,dirX,dirY);
    ConicalColor e(posx,posy,dirX,dirY);

    w.temperatura(dirX,dirY);
    c.temperatura(dirX,dirY);
    e.temperatura(dirX,dirY);

    r.show();
    return a.exec();
}
