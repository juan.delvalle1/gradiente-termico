#ifndef ELIPSECOLOR_H
#define ELIPSECOLOR_H


#include <QGraphicsEllipseItem>
#include <QObject>
#include <QPainter>
#include <QtCore>
#include <QtGui>
#include <QBrush>
#include <QPen>
#include <QColor>
#include <QPaintEvent>
#include <QLinearGradient>
#include <QRadialGradient>
#include <QConicalGradient>
#include <math.h>
#include "fadbad.h"
#include "fadiff.h"
#include "tadiff.h"


using namespace fadbad;


class ConicalColor:public QObject,public QGraphicsEllipseItem{
    Q_OBJECT
public:
    ConicalColor(int x, int y, int h, int w);
    int DirAngle;
    int x;
    int y;

signals:

public slots:
   void changeDireccionX(int DirX);

   void changeDireccionY(int DirY);

   void changeAngle(int DirX, int DirY);

   double getPuntoX();

   double getPuntoY();

   void setPuntoX(int puntoX);

   void setPuntoY(int puntoY);

   double getDireccionX();

   double getDireccionY();

   void setDireccionX(int direccionX);

   void setDireccionY(int direccionY);

   void temperatura(F<double> x, F<double> y);

private:
    void updateColor();
    int dfdx;
    int dfdy;
    int DirX;
    int DirY;
};




#endif // ELIPSECOLOR_H
