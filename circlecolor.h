#ifndef CIRCLECOLOR_H
#define CIRCLECOLOR_H

#include <QGraphicsEllipseItem>
#include <QObject>
#include <QPainter>
#include <QtCore>
#include <QtGui>
#include <QBrush>
#include <QPen>
#include <QColor>
#include <QPaintEvent>
#include <QLinearGradient>
#include <QRadialGradient>
#include <math.h>
#include "fadbad.h"
#include "fadiff.h"
#include "tadiff.h"


using namespace fadbad;

class CircleColor:public QObject,public QGraphicsEllipseItem{
    Q_OBJECT
public:
    CircleColor(int x, int y, int h, int w);
    int DirRad;
    int x;
    int y;

signals:

public slots:



   void changeDireccionX(int DirX);

   void changeDireccionY(int DirY);

   void changeDireccionRadial(int DirX, int DirY);

   double getPuntoX();

   double getPuntoY();

   void setPuntoX(int puntoX);

   void setPuntoY(int puntoY);

   double getDireccionX();

   double getDireccionY();

   void setDireccionX(int direccionX);

   void setDireccionY(int direccionY);

   void temperatura(F<double> x, F<double> y);

private:
    void updateColor();
    int dfdx;
    int dfdy;
    int DirX;
    int DirY;
};

#endif // CIRCLECOLOR_H
