#include "circlecolor.h"


CircleColor::CircleColor(int x, int y, int h, int w) :
    QGraphicsEllipseItem(x,y,h,w)
{
    this->setBrush(QBrush(Qt::black));
    this->DirRad= 0;
}

void CircleColor::changeDireccionX(int DirX){
    this->DirX= DirX;
    updateColor();

}

void CircleColor::changeDireccionY(int DirY){
    this->DirY = DirY;
    updateColor();
}

void CircleColor::changeDireccionRadial(int DirX, int DirY){
    int DirRad= sqrt(pow(DirX, 2) + pow(DirY,2));
    updateColor();
    this->DirRad= DirRad;
}

void CircleColor::updateColor(){
    QRadialGradient radialGradient(QPointF(this->DirX, this->DirY), this->DirRad ,QPointF(dfdx, dfdy));
    radialGradient.setColorAt(0, Qt::red);
    radialGradient.setColorAt(0.25,Qt::yellow);
    radialGradient.setColorAt(0.5, Qt::green);
    radialGradient.setColorAt(0.75,Qt::cyan);
    radialGradient.setColorAt(1,Qt::blue);
    this->setBrush(QBrush(radialGradient));
}
double CircleColor::getPuntoX()
{
    return dfdx;
}

double CircleColor::getPuntoY()
{
    return dfdy;
}

void CircleColor::setPuntoX(int puntoX)
{
    dfdx = puntoX;
}

void CircleColor::setPuntoY(int puntoY)
{
    dfdy= puntoY;
}

double CircleColor::getDireccionX()
{
    return DirX;
}

double CircleColor::getDireccionY()
{
    return DirY;
}

void CircleColor::setDireccionX(int direccionX)
{
    DirX= direccionX;
}

void CircleColor::setDireccionY(int direccionY)
{
    DirY= direccionY;
}

void CircleColor::temperatura(F<double> x, F<double> y)
{
    F<double> z=pow(x,3);
    F<double> p= z/5;
    F<double> r=pow(y,5);
    F<double> s= r/3;
    F<double> T=9*(s+p);

    x.diff(0,2); //diferenciacion de la funcion 0 y 2 denotan los index
    y.diff(1,2);
    double dfdx=T.d(0);//derivada parcial de x
    double dfdy=T.d(1);//derivada parcial de y
    double px= x.x();
    double py= y.x();
    this->setPuntoX(int(px));
    this->setPuntoY(int(py));
    this->setDireccionX(int(dfdx));
    this->setDireccionY(int(dfdy));
    this->changeDireccionRadial(int(dfdx),int(dfdy));
    updateColor();
}
